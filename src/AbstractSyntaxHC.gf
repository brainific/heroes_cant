-- The abstract types in our world are defined here 

abstract AbstractSyntaxHC = Common, Noun ** {
flags startcat = Sentence ;
cat Sentence ;
cat ObjectType ;
cat Object (type : ObjectType) ;
-- cat Instance (x : ObjectType) ;
cat Location ;
cat Place ;
cat Character ;
-- cat CharQuality ;
cat Occupation ;
cat ClauseType ;
cat Act ;
cat Bel (deg : Int) ;
cat Clause (c : ClauseType);
cat InLocation (p : Place);
cat ToLocation (p : Place);
-- cat Greet ;
-- cat Interject ;
-- cat Say ;
-- cat Ask ;
-- cat Request ;
-- cat Excuse ;
-- cat Belief ;
fun Weapon : ObjectType ;
fun Armor : ObjectType ;

-- Let's try and separate "location adverb" from "places"

-- fun Action : Give ;

-- cat Plan ;
-- cat Goal ;

-- fun Sure : Say -> Belief ;
-- fun Maybe : Say -> Belief ;
-- fun Doubt : Say -> Belief ;

-- fun ChainMail : Object ;
-- fun Plate : N;
-- fun Leather : N;

-- fun Spell : N;

-- fun Door : Access ;
-- fun TrapDoor : Access ;

-- fun go : Character -> Place -> Action ;
-- fun visit : Character -> Character -> Action ;
-- fun leave : Character -> Place -> Action ;

-- fun attack : Character -> Character -> Action ;

-- fun find : Character -> Character -> Action ;
-- fun find : Character -> Object -> Action ;

-- fun threaten : Character -> Character -> Action ;

-- fun kill : Character -> Character -> Action ;

-- Traditional modalities
-- fun think : State -> State ;
-- fun believe : State -> State ;
-- fun know : State -> State ;

-- fun convince : Character -> Character -> State -> Action ;
-- fun lie : Character -> Character -> State -> Action ;
-- fun tell : Character -> Character -> State -> Action ;

-- fun intend : Action ;
-- fun want : Action ;

-- fun must : Action -> State? ;
-- In the current state, <Action> must be executed next (at least)
-- fun may : Action -> State? ;
-- fun may : State -> State? ;
-- In the current state, <Action> may be executed next

-- fun should : Action -> State? ;
-- In the current state, <Action> should be executed next

-- fun fear : Character -> State -> State;

-- fun will_PN : PN;

-- fun worship : God -> Action ;

-- fun some_Quant : Quant;
-- fun how8many_IDet : IDet;
-- fun how8much_IDet : IDet;
-- fun how_IAdv : IAdv;
-- fun when_IAdv : IAdv;
-- fun where_IAdv : IAdv;
-- fun why_IAdv : IAdv;
-- fun who_N : N;
-- fun very_AdA : AdA;
-- fun all_A : A;
-- fun all_Predet : Predet;
-- fun almost_AdA : AdA;
-- fun almost_AdN : AdN;
-- fun almost_Adv : Adv;
-- fun almost_Predet : Predet;
-- fun even_AdA : AdA;
-- fun even_AdV : AdV;
-- fun even_Adv : Adv;
-- fun less_A : A;
-- fun less_AdA : AdA;
-- fun less_Adv : Adv;
-- fun less_Det : Det;
-- fun less_than_AdN : AdN;
-- fun just_A : A;
-- fun just_AdV : AdV;
-- fun just_Adv : Adv;
-- fun just_Predet : Predet;
-- fun many_A : A;
-- fun many_Det : Det;
-- fun morePl_Det : Det;
-- fun moreSg_Det : Det;
-- fun more_Adv : Adv;
-- fun more_N : N;
-- fun more_than_AdN : AdN;
-- fun more_than_Predet : Predet;
-- fun much_AdA : AdA;
-- fun much_Adv : Adv;
-- fun much_Det : Det;
-- fun much_N : N;
-- fun often_AdA : AdA;
-- fun often_AdV : AdV;
-- fun often_Adv : Adv;
-- fun only_Adv : Adv;
-- fun only_Predet : Predet;
-- fun pretty_A : A;
-- fun pretty_AdA : AdA;
-- fun pretty_N : N;
-- fun quite_AdA : AdA;
-- fun quite_Adv : Adv;
-- fun quite_Predet : Predet;
-- fun so_AdA : AdA;
-- fun so_Adv : Adv;
-- fun still_A : A;
-- fun still_AdA : AdA;
-- fun still_AdV : AdV;
-- fun still_Adv : Adv;
-- fun such_Adv : Adv;
-- fun such_Predet : Predet;
-- fun such_as_Prep : Prep;
-- fun suchlike_A : A;
-- fun too_AdA : AdA;
-- fun too_Adv : Adv;
-- fun very_AdA : AdA;

-- fun agree_with_V2 : V2;

-- fun around_AdN : AdN;
-- fun around_Adv : Adv;
-- fun around_Prep : Prep ;

-- fun under_Prep : Prep ;
-- fun until_Prep : Prep ;
-- fun until_Subj : Subj ;
-- fun up_A : A;
-- fun up_Adv : Adv;
-- fun up_Prep : Prep ;
-- fun whereas_Prep : Prep ;
-- fun whether_Prep : Prep ;
-- fun with_Prep : Prep ;
-- fun without_Prep : Prep ;
-- fun apart_from_Prep : Prep;
-- fun aside_from_Prep : Prep;
-- fun close_to_Prep : Prep;
-- fun due_to_Prep : Prep;
-- fun except_for_Prep : Prep;
-- fun left_of_Prep : Prep;
-- fun near_to_Prep : Prep;
-- fun outside_of_Prep : Prep;
-- fun right_of_Prep : Prep;
-- fun in_case_of_Prep : Prep;
-- fun in_front_of_Prep : Prep;

-- fun because_Subj : Subj;
-- fun because_of_Prep : Prep;
-- fun before_Adv : Adv;
-- fun before_Prep : Prep ;
-- fun before_Subj : Subj ;
-- fun behind_Prep : Prep ;
-- fun behind_Adv : Adv;
-- fun below_Adv : Adv;
-- fun below_Prep : Prep ;
-- fun beside_Prep : Prep ;
-- fun besides_Adv : Adv;
-- fun besides_Prep : Prep ;
-- fun between_Adv : Adv;
-- fun between_Prep : Prep;
-- fun but_Adv : Adv;
-- fun but_PConj : PConj;
-- fun but_Prep : Prep;
-- fun but_Subj : Subj;
-- fun despite_N : N;
-- fun despite_Prep : Prep ;
-- fun down_A : A;
-- fun down_Adv : Adv;
-- fun down_Prep : Prep ;
-- fun during_Prep : Prep ;
-- fun except_Prep : Prep ;
-- fun for_Prep : Prep;
-- fun from_Prep : Prep ;
-- fun in_A : A;
-- fun in_Adv : Adv;
-- fun in_Prep : Prep;
-- fun inside_A : A;
-- fun inside_Adv : Adv;
-- fun inside_Prep : Prep ;
-- fun instead_Adv : Adv;
-- fun instead_of_Prep : Prep;
-- fun into_Prep : Prep ;
-- fun near_A : A;
-- fun near_Adv : Adv;
-- fun near_Prep : Prep ;
-- fun neither_A : A;
-- fun neither_Adv : Adv;
-- fun neither_Det : Det;
-- fun neither_Prep : Prep ;
-- fun next_A : A;
-- fun next_Prep : Prep ;
-- fun of_Prep : Prep ;
-- fun off_A : A;
-- fun off_Adv : Adv;
-- fun off_Prep : Prep ;
-- fun on_Adv : Adv;
-- fun on_Prep : Prep ;
-- fun once_AdV : AdV;
-- fun once_Adv : Adv;
-- fun once_Prep : Prep ;
-- fun once_Subj : Subj;
-- fun opposite_A : A;
-- fun opposite_Prep : Prep ;
-- fun out_A : A;
-- fun out_Adv : Adv;
-- fun out_Prep : Prep ;
-- fun outside_A : A;
-- fun outside_Adv : Adv;
-- fun outside_Prep : Prep ;
-- fun over_Adv : Adv;
-- fun over_Prep : Prep ;
-- fun past_A : A;
-- fun past_Adv : Adv;
-- fun past_Prep : Prep ;
-- fun pending_Prep : Prep ;
-- fun since_Adv : Adv;
-- fun since_Prep : Prep;
-- fun since_Subj : Subj;
-- fun such_Adv : Adv;
-- fun such_Predet : Predet;
-- fun such_as_Prep : Prep;
-- fun then_Prep : Prep ;
-- fun though_Adv : Adv;
-- fun though_Prep : Prep;
-- fun though_Subj : Subj;
-- fun through_Adv : Adv;
-- fun through_Prep : Prep ;
-- fun till_Prep : Prep ;
-- fun to_Adv : Adv;
-- fun to_Prep : Prep;
-- fun toward_Prep : Prep ;
-- fun towards_Prep : Prep ;
-- fun like_A : A;
-- fun like_Adv : Adv;
-- fun like_Prep : Prep ;
-- fun like_V2 : V2;
-- fun like_V2V : V2V;
-- fun like_VS : VS;
-- fun like_VV : VV;
-- fun at_Prep : Prep ;
-- fun as_Prep : Prep;
-- fun as_Subj : Subj;
-- fun as_long_as_Subj : Subj;
-- fun as_well_Adv : Adv;

-- fun and_Conj : Conj;
-- fun as_well_as_Conj : Conj;
-- fun as_long_as_Subj : Subj;
-- fun either7or_DConj : Conj;
-- fun neither7nor_DConj : Conj;
-- fun nor_Conj : Conj;
-- fun or_Conj : Conj;
-- fun rather_than_Conj : Conj;



-- fun Trustworthy : CharQuality ;
-- fun Friend : Character -> CharQuality ;
-- fun Someone : Character ;
-- fun Anyone : Character ;

fun
  -- Simple objects
  -- FromOccupation : (x : Det) -> Occupation -> Character;
  FromOccupation : Det -> Occupation -> Character;
  TheDet   : Det ;
  ADet     : Det ;
  -- NoDet    : Det ;
  Someone : Character ;
  Alice : Character ;
  Bob : Character ;
  Carol : Character ;
  Aisha : Character ;
  BoYang : Character ;
  Chinira : Character ;
  Sword : Object Weapon ;
  Axe : Object Weapon ;
  Mace : Object Weapon ;
  Spear : Object Weapon ;
  Dagger : Object Weapon ;
  ChainMail : Object Armor ;
  Plate : Object Armor ;
  Leather : Object Armor ;
  Shield : Object Armor ;
  Wizard : Occupation ;
  Thief : Occupation ;
  Priest : Occupation ;
  Warrior : Occupation ;
  
  -- LOCATIONS START
  -- Pointing : (coords : String) -> Place ;
  -- Here : Place ;
  -- Cave : Location ;
  -- Palace : Location ;
  -- House : Location ;
  -- SomePlace : Det -> Location -> Place ;
  -- InPlace : (p : Place) -> InLocation p; 
  -- ToPlace : (p : Place) -> ToLocation p;
  -- LOCATIONS END
   
  -- States : FOL / epistemic statements
  -- buildNegSentence
  -- build
  Action : ClauseType;
  State : ClauseType;
  PastSentence : (c : ClauseType) -> Clause c -> Sentence;
  PresentSentence : (c : ClauseType) -> Clause c -> Sentence;
  -- ObjInstance : (d : Det) -> (x : ObjectType) -> Object x -> Instance x;
  Has : Det -> (x : ObjectType) -> Character -> Object x -> Clause State;
  IsA : Character -> Occupation -> Clause State;
  -- Actions : dynamic style, with relations attached
  -- Needs tense, polarity and anteriority
  KillWithAct : Det -> Character -> Object Weapon -> Act ;
  KillAct : Character -> Act ;
  TellAct : Character -> Sentence -> Act ;
  -- Action of telling someone some event, can be recursive
  -------------
  -- Turn an action into a state using the progressive
  ProgrAction : Character -> Act -> Clause State ;
  ConcrAction : Character -> Act -> Clause Action ;
  DynamicMod1 : Clause Action -> Clause State -> Sentence ;
  DynamicMod2 : Clause Action -> Clause Action -> Sentence ;
  DoxasticMod : Character -> (deg : Int) -> Bel deg -> Sentence -> Clause State ;
  Believe : Bel 0 ;
  Suspect : Bel 1 ;
  Doubt : Bel 2 ;
}
