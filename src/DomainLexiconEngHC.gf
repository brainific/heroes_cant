instance DomainLexiconEngHC of DomainLexiconHC = CatEng ** open ResEng, NounEng, ParadigmsEng, StructuralEng in {
oper
  sword_N = mkN "sword" "swords";
  axe_N = mkN "axe" "axes";
  mace_N = mkN "mace" "maces";
  spear_N = mkN "spear" "spears";
  dagger_N = mkN "dagger" "daggers";
  shield_N = mkN "shield" "shields";
  
  wizard_N = mkN human (mkN "wizard" "wizards");
  thief_N = mkN "thief" "thieves";
  priest_N = mkN "priest" "priests";
  warrior_N = mkN "warrior" "warriors";

  chain_mail_N = mkN "chainmail";
  plate_N = mkN "plate";
  armor_N = mkN "armor";
  weapon_N = mkN "weapon";
  leather_N = mkN "leather";
  
  cave_N = mkN "cave" "caves"; 
  palace_N = mkN "palace" "palaces";
  house_N = mkN "house" "houses";
  here_N = mkN "here";

  alice_PN = mkPN "Alice";
  bob_PN = mkPN "Bob";
  carol_PN = mkPN "Carol";

  kill_V = mkV "kill" "kills" "killed" "killed" "killing";
  kill_V2 = mkV2 (mkV "kill" "kills" "killed" "killed" "killing");
  talk_V2 = mkV2 (mkV "talk" "talks" "talked" "talked" "talking");
  suspect_VS = mkVS (mkV "suspect");  
  doubt_VS = mkVS (mkV "doubt");
  believe_VS = mkVS (mkV "believe" "believes" "believed" "believed" "believing");
  
  about_Prep = mkPrep "about";
  
  thereWithPos_Adv place = mkAdv ("there <" ++ place.s ++ ">") ;

  someone_NP = mkNP "someone" "someone" "someone's" Sg P3 human;
  
}