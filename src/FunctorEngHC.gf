--# -path=.:alltenses:prelude

concrete FunctorEngHC of AbstractSyntaxHC = FunctorHC with
  (Syntax = SyntaxEng),
  (DomainLexiconHC = DomainLexiconEngHC),
  (Constructors = ConstructorsEng),
  (Noun = NounEng) ;