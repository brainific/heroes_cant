incomplete concrete FunctorHC of AbstractSyntaxHC = open Constructors,
  Syntax, DomainLexiconHC, ParadigmsEng, IrregEng, Noun in {
lincat 
  Sentence = S;
  Character = NP;
  Instance = NP;
  Object = N;
  ObjectType = N;
  Occupation = N;
  Place = N;
  Location = N;
  Clause = Cl;
  ClauseType = {s : Str};
  Det = Det;
  InLocation = Adv;
  ToLocation = Adv;
  Act = VP;
  Bel = VS;
lin
  PastSentence _ cl = mkS pastTense cl;
  PresentSentence _ cl = mkS presentTense cl;
  ProgrAction ch act = mkCl ch (progressiveVP act);
  ConcrAction ch act = mkCl ch act;
  ObjInstance det _ obj = mkNP det obj;
  TheDet = the_Det ;
  ADet = a_Det ;
  Has det _ ch obj = mkCl ch (mkVP have_V2 (mkNP det obj)) ;
  IsA ch occ = mkCl ch (mkNP a_Det occ) ;
  -- KillWithAct ch2 obj = mkVP (mkVP kill_V2 ch2) (ConstructorsEng.mkAdv with_Prep (mkNP obj)) ;
  KillWithAct det ch2 obj = mkVP (mkVP kill_V2 ch2) (ConstructorsEng.mkAdv with_Prep (mkNP det obj)) ;
  KillAct ch2 = mkVP kill_V2 ch2 ;
  TellAct ch2 tht = mkVP (mkV2S (IrregEng.tell_V) noPrep) ch2 tht;
  DynamicMod1 what then = mkS if_then_Conj (mkS presentTense what) (mkS futureTense then);
  DynamicMod2 what then = mkS if_then_Conj (mkS presentTense what) (mkS futureTense then);
  DoxasticMod ch _ bel what = mkCl ch bel what;
  Believe = believe_VS ;
  Suspect = suspect_VS ;
  Doubt = doubt_VS ;
  Alice = mkNP alice_PN;
  Bob = mkNP bob_PN;
  Carol = mkNP carol_PN;
  Someone = someone_NP;
  Armor = armor_N ;
  Weapon = weapon_N ;
  State = { s = [] } ;
  Action = { s = [] } ;
  ChainMail = chain_mail_N ;
  Plate = plate_N ;
  Leather = leather_N ;
  Shield = shield_N ;
  Sword = sword_N ;
  Axe = axe_N;
  Mace = mace_N;
  Spear = spear_N;
  Dagger = dagger_N;
  Wizard = wizard_N;
  Thief = thief_N;
  Priest = priest_N;
  Warrior = warrior_N;
  Cave = cave_N;  
  Palace = palace_N;
  House = house_N;
  FromOccupation det inst = mkNP det inst ;

  -- InPlace p = ConstructorsEng.mkAdv in_Prep (mkNP isDet p) ; 
  -- ToPlace p = ConstructorsEng.mkAdv to_Prep (mkNP isDet p) ;
  -- Pointing : (coords : String) -> Place ;
  -- Here : Place ;
  -- Cave : Location ;
  -- Palace : Location ;
  -- House : Location ;
  -- SomePlace : Det -> Location -> Place ;
  -- InPlace : (p : Place) -> InLocation p; 
  -- ToPlace : (p : Place) -> ToLocation p; 
}